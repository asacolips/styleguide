Styleguide - HTML Mockups with kss-node
==========

## Getting Started
- add `192.168.34.19 styleguide.dev` to your **/etc/hosts** file
- `vagrant up`
- cd into **/docroot/public**
- `npm install`
- `npm run gulp`
- `npm run gulp styleguide`
- `npm run gulp compress`
- go to http://styleguide.dev/styleguide/dist/default/index.html to view the generated style guide
- go to http://styleguide.dev/styleguide/dist/basic/index.html to view the compiled basic HTML page mockup